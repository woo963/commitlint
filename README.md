npm install
npm install --save-dev @commitlint/cli commitlint-plugin-jira-rules commitlint-config-jira
npx husky install
npx husky add .husky/commit-msg "npx --no-install commitlint --edit $1"

Добавленное правило лежит в commitlint.config.js

Пример работы: 

C:\...\commitlint>git commit -m "Test message"
⧗   input: Test message
✖   Commit message parts must be separated with ":" e.g: IB-2121:My commit message body [jira-commit-message-separator]
✖   the commit message must provide minimum one task id followed by (:) symbol, if task not have an id use a conventional task id e.g: "IB-0000: My commit message" [jira-task-id-empty]
✖   null taskId must start with project key RB [jira-task-id-project-key]

✖   found 3 problems, 0 warnings
ⓘ   Get help: https://github.com/conventional-changelog/commitlint/#what-is-commitlint

husky - commit-msg hook exited with code 1 (error)

C:\...\commitlint>git commit -m "RB-34: Test message"
[master ec9ad69] RB-34: Test message
 2 files changed, 6 insertions(+), 2 deletions(-)